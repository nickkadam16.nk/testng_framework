package API_Utility_Common_Methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Extractor {

	public static ArrayList<String> Excel_Data_Reader(String fileName, String sheetName, String tcName)
			throws IOException {

		ArrayList<String> arr_data = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");

//		step 1: create the object of file input stream to locate the test data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Test_Data\\" + fileName + ".xlsx");

//		step 2: create the XSSFWorkbook object to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

//		step 3: fetch the number of sheets available in the excel file
		int count = wb.getNumberOfSheets();

//		step 4: access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheet_name = wb.getSheetName(i);
			if (sheet_name.equals(sheetName)) {
//				System.out.println("sheet name: " + sheet_name);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row data_row = row.next();
					String tc_name = data_row.getCell(0).getStringCellValue();
//					System.out.println("iterate bet tc name: " + tc_name);
					if (tc_name.equals(tcName)) {
//						System.out.println("required tc name is: " + tc_name);
						Iterator<Cell> cell_values = data_row.iterator();
						while (cell_values.hasNext()) {
							String test_cell_data = cell_values.next().getStringCellValue();
//							System.out.println("test cell data is: " + test_cell_data);
							arr_data.add(test_cell_data);
						}
						break;
					}
				}
			}
		}
		wb.close();
		return arr_data;
	}
}
