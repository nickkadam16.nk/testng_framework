package TestNG_Data_Provider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_Method_Handle;
import API_Endpoints.Request_Endpoints;
import API_Request_Repo.TestNG_Data_Provider;
import API_Utility_Common_Methods.Handle_API_Logs;
import API_Utility_Common_Methods.Handle_Dir;
import io.restassured.path.json.JsonPath;

public class DD_Diff_Class_Post_Method extends Common_Method_Handle{

	static File logDir;
	static String request_body;
	static String end_point;
	static String response_body;
	
	@BeforeTest
	public static void test_setup() throws IOException {
		logDir = Handle_Dir.create_log_dir("Post_Request_Log_Dir");
		end_point = Request_Endpoints.post_endpoint();
	}

	@Test(dataProvider = "post_data_provider", dataProviderClass = TestNG_Data_Provider.class)
	public static void post_executor(String name, String job) throws IOException {
		request_body = "{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int status_code = post_statusCode(end_point, request_body);
			System.out.println(status_code);
			if (status_code == 201) {
				String response_body = post_responseBody(end_point, request_body);
				System.out.println(response_body);
				DD_Diff_Class_Post_Method.validator(request_body, response_body);
				break;
			} else {
				System.out.println("expected status code is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
//		parse the request body and response body
		JsonPath JPReq = new JsonPath(requestBody);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(responseBody);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0, 11);

//		validate the response body
		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);
	}

	@AfterTest
	public void test_teardown() throws IOException {
		String Post_Request_Log_File = this.getClass().getName();
		Handle_API_Logs.evidence_creator(logDir, Post_Request_Log_File, request_body, end_point, response_body);
	}
	
}
