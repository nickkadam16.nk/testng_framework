package Rest_API;

import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Post_API {

	public static void main(String[] args) {
		
//		declare the base URL
		RestAssured.baseURI = "https://reqres.in/";
		
//		Configure the request body and trigger the API and capture the response
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		String responseBody = given().header("Content-Type","application/json").body(requestBody)
		.when().post("api/users")
		.then().extract().response().asString();
		System.out.println(responseBody);
		
//		parse the request body and response body
		JsonPath JPReq = new JsonPath(requestBody);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0,11);

		JsonPath JPRes = new JsonPath(responseBody);
		int resId = JPRes.getInt("id");
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("createdAt").substring(0,11);
		
//		validate the response body
		Assert.assertNotNull(resId);
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);
			
	}

}
