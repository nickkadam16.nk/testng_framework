package Rest_API;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get1_API {
	
	public static void main(String[] args) {
		
//		declare the base URL
		RestAssured.baseURI = "https://reqres.in/";
		
//		trigger the API and capture the response body
		String responseBody = given()
		.when().get("api/users?page=1")
		.then().extract().response().asString();
//		System.out.println(responseBody);
		
//		expected data
		int exp_id[] = {1,2,3,4,5,6};
		String exp_email[] = {"george.bluth@reqres.in","janet.weaver@reqres.in","emma.wong@reqres.in","eve.holt@reqres.in",
				"charles.morris@reqres.in","tracey.ramos@reqres.in"};
		String exp_fName[] = {"George","Janet","Emma","Eve","Charles","Tracey"};
		String exp_lName[] = {"Bluth","Weaver","Wong","Holt","Morris","Ramos"};
		
//		parse the response body
		JSONObject JO = new JSONObject(responseBody);
//		System.out.println(JO);
		JSONArray JArr = JO.getJSONArray("data");
//		System.out.println(JArr);
		int count = JArr.length();
//		System.out.println(count);
		
		for(int i=0;i<count;i++) {
			
			int res_id = JArr.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			String res_email = JArr.getJSONObject(i).getString("email");
			String res_fName = JArr.getJSONObject(i).getString("first_name");
			String res_lName = JArr.getJSONObject(i).getString("last_name");
			
//			validate the response body
			Assert.assertEquals(exp_id[i], res_id);
			Assert.assertEquals(exp_email[i], res_email);
			Assert.assertEquals(exp_fName[i], res_fName);
			Assert.assertEquals(exp_lName[i], res_lName);
				
		}
		
	}
}
