package API_Methods;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_Method_Handle;
import API_Endpoints.Request_Endpoints;
import API_Utility_Common_Methods.Handle_API_Logs;
import API_Utility_Common_Methods.Handle_Dir;

public class Get2_Request_Method extends Common_Method_Handle {

	static File logDir;
	static String request_body;
	static String end_point;
	static String response_body;

	@BeforeTest
	public static void test_setup() {
		logDir = Handle_Dir.create_log_dir("Get2_Request_Log_Dir");
		request_body = null;
		end_point = Request_Endpoints.get2_endpoint();
	}

	@Test(description = "::::::::::executing the Get2 API and validating the response body::::::::::")
	public static void get2_executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			int status_code = get_statusCode(end_point);
			System.out.println(status_code);
			if (status_code == 200) {
				response_body = get_responseBody(end_point);
//				System.out.println(response_body);
				Get2_Request_Method.validator(response_body);
				break;
			} else {
				System.out.println("expected status code is not found hence retrying");
			}
		}
	}

	public static void validator(String responseBody) {

//		expected data
		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String exp_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_fName[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_lName[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

//		parse the response body
		JSONObject JO = new JSONObject(responseBody);
		JSONArray JArr = JO.getJSONArray("data");
		int count = JArr.length();

		for (int i = 0; i < count; i++) {
			int res_id = JArr.getJSONObject(i).getInt("id");
			String res_email = JArr.getJSONObject(i).getString("email");
			String res_fName = JArr.getJSONObject(i).getString("first_name");
			String res_lName = JArr.getJSONObject(i).getString("last_name");

//			validate the response body
			Assert.assertEquals(exp_id[i], res_id);
			Assert.assertEquals(exp_email[i], res_email);
			Assert.assertEquals(exp_fName[i], res_fName);
			Assert.assertEquals(exp_lName[i], res_lName);
		}
	}

	@AfterTest
	public void test_teardown() throws IOException {
		String Get2_Request_Log_File = this.getClass().getName();
		Handle_API_Logs.evidence_creator(logDir, Get2_Request_Log_File, request_body, end_point, response_body);
	}

}
