package API_Methods;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_Method_Handle;
import API_Endpoints.Request_Endpoints;
import API_Request_Repo.Put_Request_Repo;
import API_Utility_Common_Methods.Handle_API_Logs;
import API_Utility_Common_Methods.Handle_Dir;
import io.restassured.path.json.JsonPath;

public class Put_Request_Method extends Common_Method_Handle {

	static File logDir;
	static String request_body;
	static String end_point;
	static String response_body;

	@BeforeTest
	public static void test_setup() throws IOException {
		logDir = Handle_Dir.create_log_dir("Put_Request_Log_Dir");
		request_body = Put_Request_Repo.put_requestBody();
		end_point = Request_Endpoints.put_endpoint();
	}

	@Test(description = "::::::::::executing the Put API and validating the response body::::::::::")
	public static void put_executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			int status_code = put_statusCode(end_point, request_body);
			System.out.println(status_code);
			if (status_code == 200) {
				response_body = put_responseBody(end_point, request_body);
//				System.out.println(response_body);
				Put_Request_Method.validator(request_body, response_body);
				break;
			} else {
				System.out.println("expected status code is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
//		parse the request body and response body
		JsonPath JPReq = new JsonPath(requestBody);
		String reqName = JPReq.getString("name");
		String reqJob = JPReq.getString("job");
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);

		JsonPath JPRes = new JsonPath(responseBody);
		String resName = JPRes.getString("name");
		String resJob = JPRes.getString("job");
		String resDate = JPRes.getString("updatedAt").substring(0, 11);

//		validate the response body
		Assert.assertEquals(reqName, resName);
		Assert.assertEquals(reqJob, resJob);
		Assert.assertEquals(exp_date, resDate);
	}

	@AfterTest
	public void test_teardown() throws IOException {
		String Put_Request_Log_File = this.getClass().getName();
		Handle_API_Logs.evidence_creator(logDir, Put_Request_Log_File, request_body, end_point, response_body);
	}
}
