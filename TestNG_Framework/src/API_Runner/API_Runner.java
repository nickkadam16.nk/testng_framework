package API_Runner;

import java.io.IOException;
import API_Methods.Get1_Request_Method;
import API_Methods.Get2_Request_Method;
import API_Methods.Post_Request_Method;
import API_Methods.Put_Request_Method;

public class API_Runner {

	public static void main(String[] args) throws IOException {
		Get1_Request_Method.get1_executor();
		Get2_Request_Method.get2_executor();
		Post_Request_Method.post_executor();
		Put_Request_Method.put_executor();
	}
}
