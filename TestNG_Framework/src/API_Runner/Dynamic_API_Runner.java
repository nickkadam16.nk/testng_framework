package API_Runner;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import API_Utility_Common_Methods.Excel_Data_Extractor;

public class Dynamic_API_Runner {
	
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		ArrayList<String> tc_runner = Excel_Data_Extractor.Excel_Data_Reader("TestData", "Test_Cases", "TC_Name");
		System.out.println(tc_runner);

		int count = tc_runner.size();
		for (int i = 1; i < count; i++) {
			String tc_name = tc_runner.get(i);
			System.out.println(tc_name);

//			call the test script class on runtime by using java.lang.reflect package
			Class<?> test_class = Class.forName("API_Methods." + tc_name);

//			call the execute method belonging to test class captured in variable tc_name
//			by using java.lang.reflect.method class
			Method execute_method = test_class.getDeclaredMethod("executor");

//			set the accessibility of method true
			execute_method.setAccessible(true);

//			create the instance of test class captured in variable name test_class
			Object instance_of_test_class = test_class.getDeclaredConstructor().newInstance();

//			execute the test script class fetch in the variable test_class
			execute_method.invoke(instance_of_test_class);
		}
	}
}
