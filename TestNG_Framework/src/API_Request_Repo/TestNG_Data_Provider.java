package API_Request_Repo;

import org.testng.annotations.DataProvider;

public class TestNG_Data_Provider {

	@DataProvider()
	public Object[][] post_data_provider(){
		return new Object[][]
				{
					{"Vijay","Native"},
					{"Akshay","Flutter"},
					{"Ashish","Vue"}
				};
	}
	
	public Object[][] put_data_provider(){
		return new Object[][] 
				{
					{"Apurva","QA"},
					{"Kavita","HR"},
					{"Sanjana","Accountant"}
				};
	}
	
	
	
	
	
}
