package API_Request_Repo;

import java.io.IOException;
import java.util.ArrayList;
import API_Utility_Common_Methods.Excel_Data_Extractor;

public class Put_Request_Repo {

	public static String put_requestBody() throws IOException {

		ArrayList<String> data = Excel_Data_Extractor.Excel_Data_Reader("TestData", "Put_API", "Put_Tc3");
//		System.out.println(data);

		String name = data.get(1);
		String job = data.get(2);

		String requestBody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestBody;
	}
}
