package API_Endpoints;

public class Request_Endpoints {

	public static String get1_endpoint() {
		String endpoint = "https://reqres.in/api/users?page=1";
		return endpoint;
	}

	public static String get2_endpoint() {
		String endpoint = "https://reqres.in/api/users?page=2";
		return endpoint;
	}

	public static String post_endpoint() {
		String endpoint = "https://reqres.in/api/users";
		return endpoint;
	}

	public static String put_endpoint() {
		String endpoint = "https://reqres.in/api/users/2";
		return endpoint;
	}
}
