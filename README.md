This is my TestNG Framework
my framework is a combination of keyword driven framework and wherein i'm having test scripts, test cases which are also being executed on the basis of data.
my framework can be divided into six major components

- TestNG.xml file for running test cases
- Test Scripts
- Common Functions
- Data file/ Variable file / Excel File / Parameterization
- Libraries
- Reports

And for running the test cases i've converted the test scripts into TestNG.

in TestNg.xml file we fetched test cases to be executed from an excel file and call the corresponding test cases on runtime by using @Test Annotations.

and I've used @BeforeTest for calling prerequisites for e.g Creating the log directory, creating the log files, Read data from excel and construct Request body and Endpoint which is called before the @Test method.

and I've used @AfterTest for Used to define the method which will be executed post the @Test method is done for e.g. Evidence file creation (Evidencefilecreator).

then we have common functions in this we have two categories
i) API related common functions
ii) Utilities

in API related common functions we have the common functions which are use to trigger the API extract the response body and extract the status code.

then we have Utilities section under the utilities we create utilities to create the directory when it is does not exists for test log files.

and if at all log directory exists we deleted and created new log directory.

then we have one more utility which will read the values from an excel file.

then we are using excel file to input the data to our framework or the requestbody parameters are entered into a excel file.

and then we have common function which will take automatically take the endpoint, requestbody responsebody and respected headers and added into a notepad file for each single test script.

I've also used maven surefire plugin so anyone can run my test cases without using Eclipse IDE.

also I've created Master Suite File and added the suite files (which contains testng.xml files of Sprint1 and Sprint2) according to Sprint wise.

then the libraries which are using are rest assured, testNG, org.json, apachePOI.

And for generating reports we are using allure-testNG, extentreports libraries.

the dependency management library are been handled by maven repository.
